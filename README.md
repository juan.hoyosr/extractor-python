# extractor-python

Extracts the toggle components of a Python or Django template files.

## Setup

```sh
$ pip install -r requirements.txt
```

## Test

```sh
$ python -m unittest discover
```

# Usage

```sh
$ python extractor-python.py --directory ~/site/my_app/ --filepath views.py --library django-waffle
```

Notice the dashes in the library name will be converted to underscores to match proper python package naming syntax.

## Build

```sh
$ pyinstaller --onefile extractor-python.py
$ PATH=`pwd`/dist/:$PATH
$ extractor-python --help
```

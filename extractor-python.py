import argparse
import sys
from file_parser import parse
from ipc_parser import ipc_parse

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Extract the toggles of a file in a specified directory using a toggling library')
    parser.add_argument('--directory')
    parser.add_argument('--filepath')
    parser.add_argument('--library')

    subparsers = parser.add_subparsers(title='subcommands', dest='ipc')
    ipc_parser = subparsers.add_parser('ipc', help='Extracts toggles through an IPC channel with another process via stdio')

    args = parser.parse_args()
    if args.directory:
        sys.stdout.write(parse(args.filepath, args.directory, args.library))
        sys.stdout.flush()
    elif args.ipc:
        ipc_parse()

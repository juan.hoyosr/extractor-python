import ast
from .loader import WaffleNode
from .components import Router, Point

# Collects the ToggleAreas from an AST node

class BaseCollector():
    def __init__(self, filename):
        self.filename = filename
        self.toggles = []

    def get_toggles(self):
        raise NotImplementedError('get_toggles must be implemented by subclasses')

class PyAstCollector(BaseCollector, ast.NodeVisitor):
    def __init__(self, filename, an_ast):
        super(PyAstCollector, self).__init__(filename)
        self.an_ast = an_ast

    def get_range(self, line, column):
        return {
            'line': line,
            'column': column,
        }

    def get_toggle_area_kwargs(self, node):
        ranges = self.get_area_ranges(node)
        kwargs = {
            'file': self.filename,
            'start': ranges['start'],
            'end': ranges['end'],
            'an_ast': node
        }

        return kwargs

    def get_area_ranges(self, node):
        ast.fix_missing_locations(node)

        lowest_node = node
        for descendant in ast.walk(node):
            if hasattr(descendant, 'lineno') and descendant.lineno > lowest_node.lineno:
                lowest_node = descendant

        return {
            'start': self.get_range(node.lineno, node.col_offset),
            'end': self.get_range(lowest_node.lineno, lowest_node.col_offset),
        }

    def visit_Call(self, node):
        if Router.is_router(node):
            kwargs = self.get_toggle_area_kwargs(node)
            self.toggles.append(Router(**kwargs))

        self.generic_visit(node)

    def visit_If(self, node):
        if Point.is_point(node):
            kwargs = self.get_toggle_area_kwargs(node)
            self.toggles.append(Point(**kwargs))

        self.generic_visit(node)

    def visit_FunctionDef(self, node):
        if Point.is_point(node):
            kwargs = self.get_toggle_area_kwargs(node)
            self.toggles.append(Point(**kwargs))

        self.generic_visit(node)

    def get_toggles(self):
        self.visit(self.an_ast)
        return self.toggles

class DjangoTemplateCollector(BaseCollector):
    def __init__(self, filename, template):
        super(DjangoTemplateCollector, self).__init__(filename)
        self.template = template

    @classmethod
    def get_all_descendants(self, node):
        nodes = []
        for attr in node.child_nodelists:
            nodelist = getattr(node, attr, None)
            if nodelist:
                for child_node in nodelist:
                    nodes.append(child_node)
                    nodes.extend(self.get_all_descendants(child_node))

        return nodes

    @classmethod
    def find_lowest_node(self, node):
        lowest = node
        for descendant in self.get_all_descendants(node):
            if lowest.token.lineno <= descendant.token.lineno:
                lowest = descendant

        return lowest

    def get_end_line(self, node, is_point):
        if not is_point:
            return node.token.lineno

        lowest = self.find_lowest_node(node)
        lowest_lineno = lowest.token.lineno

        if type(lowest).__name__ != 'TextNode':
            adjusted_lowest_lineno = lowest_lineno
        else:
            adjusted_lowest_lineno = lowest_lineno + lowest.s.count('\n')

        return adjusted_lowest_lineno


    def get_toggle_area_kwargs(self, node, is_point = False):
        kwargs = {
            'file': self.filename,
            'start': {
                'line': node.token.lineno,
            },
            'end': {
                'line': self.get_end_line(node, is_point),
            },
            'an_ast': node
        }

        return kwargs

    def get_toggles(self):
        for node in self.template.nodelist.get_nodes_by_type(WaffleNode):
            if Point.is_point(node):
                kwargs = self.get_toggle_area_kwargs(node, True)
                self.toggles.append(Point(**kwargs))

            if Router.is_router(node):
                kwargs = self.get_toggle_area_kwargs(node, False)
                self.toggles.append(Router(**kwargs))

        return self.toggles
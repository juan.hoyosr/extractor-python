import django
from django import template
from django.conf import settings

django_config = {
    'INSTALLED_APPS': [
        # default django apps
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',

        'waffle'
    ],
    'TEMPLATES': [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates'
        }
    ],
}

settings.configure(**django_config)
django.setup()

from waffle.templatetags.waffle_tags import WaffleNode
WaffleNode

# ----------------------------------------
# Bypass unknown Parser libraries and tags
# ----------------------------------------
from django.template.base import TextNode, Parser, FilterExpression
from collections import UserDict

def mocked_tag(parser, token):
    return TextNode('')

def mocked_filter(*args):
    return

class MyDict(UserDict):
    def __init__(self, initdata, a_type):
        self.a_type = a_type
        super(MyDict, self).__init__(initdata)

    def __getitem__(self, key):
        if key not in self.data:
            if self.a_type == 'tags':
                return mocked_tag
            elif self.a_type == 'filters':
                return mocked_filter
            elif self.a_type == 'libraries':
                return MockedLibrary()
        else:
            return super().__getitem__(key)

    def __contains__(self, item):
        return True

class MockedLibrary():
    def __init__(self):
        self.tags = MyDict({}, 'tags')
        self.filters = MyDict({}, 'filters')

original_parser__init__ = Parser.__init__

def parser__init__(self, *args, **kwargs):
    original_parser__init__(self, *args, **kwargs)
    if not isinstance(self.tags, MyDict):
        self.tags = MyDict(self.tags, 'tags')

    if not isinstance(self.filters, MyDict):
        self.filters = MyDict(self.filters, 'filters')

    if not isinstance(self.libraries, MyDict):
        self.libraries = MyDict(self.libraries, 'libraries')

Parser.__init__ = parser__init__

def mocked_args_check(*args):
    return True

# mocked_filter does not specify the args, so
# we need to disable the check
FilterExpression.args_check = mocked_args_check

def load_django_template(source):
    return template.Template(source)

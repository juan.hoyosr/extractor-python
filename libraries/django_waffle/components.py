import hashlib
import ast
import astor
import json
import copy
import re
from .loader import WaffleNode

ENCODING = 'utf-8'

class ToggleComponent():
    def __init__(self):
        self.type = self.__class__.__name__

        # unique across all usages
        self.id = self.id()

    def id(self):
        if self.type != 'ToggleComponent' and self.type != 'ToggleArea':
            raise NotImplementedError('id() must be implemented')


class ToggleArea(ToggleComponent):
    def __init__(self, file, start, end, an_ast):
        self.file = file
        self.start = start
        self.end = end
        self.ast = an_ast

        # repeated in all instances across all usages
        self.common_id = self.common_id()

        # The hash is useful to compare components with a higher degree of granularity.
        # It is used instead of the common_id, despite its similarity (e.g. ast + no location
        # fingerprints), because the common_id can strip valuable parts of the ast that
        # could be needed to check, for example, if the same component has been moved
        # to another line.
        self.hash = self.hash()

        super(ToggleArea, self).__init__()

    def common_id(self):
        if self.__class__.__name__ != 'ToggleArea':
            raise NotImplementedError('common_id() must be implemented')

    def hash(self):
        hash = hashlib.sha256()
        hash.update(bytes(ToggleArea.ast_as_string(self.ast, with_positions=False), ENCODING))
        return hash.hexdigest()

    @classmethod
    def isasttype(self, an_ast, a_type):
        package_type = {
            'py': 'ast',
            'django-template': 'waffle.templatetags.waffle_tags',
        }

        klass = None
        class_name = an_ast.__class__.__name__
        module = __import__(package_type[a_type], fromlist=[class_name])
        try:
            klass = getattr(module, class_name)
        except AttributeError:
            pass

        if klass is None:
            return False

        return isinstance(an_ast, klass)

    @classmethod
    def get_o2json_parser(self, ignore=[]):
        def o2json(object):
            if hasattr(object, '__dict__'):
                return { key: val for key, val in object.__dict__.items() if key not in ignore }
            else:
                return str(object)

        return o2json

    @classmethod
    def ast_as_string(self, an_ast, with_positions=True):
        if ToggleArea.isasttype(an_ast, 'py'):
            ignore = []
            if not with_positions:
                ignore.extend(['lineno', 'col_offset'])
            return json.dumps(an_ast, default=ToggleArea.get_o2json_parser(ignore))
        elif ToggleArea.isasttype(an_ast, 'django-template'):
            # Ignoring some entries avoids '<[type] [Class] at 0x102000a60>'
            # serializations and useless "ast" nodes. Good to increase determinism.
            ignore = ['__objclass__']
            if not with_positions:
                ignore.extend(['lineno', 'position'])
            return json.dumps(an_ast, default=ToggleArea.get_o2json_parser(ignore))


class Declaration(ToggleComponent):
    def __init__(self, name, environment, waffle_type):
        self.name = name
        self.environment = environment
        self.waffle_type = waffle_type

        super(Declaration, self).__init__()

    def id(self):
        id_data = {
            'name': self.name,
            'environment': self.environment,
            'waffle_type': self.waffle_type,
        }
        return '{name}-{environment}-{waffle_type}'.format(**id_data)


WAFFLE_ROUTERS = set([
    'flag_is_active',
    'switch_is_active',
    'sample_is_active',
    'waffle_flag',
    'waffle_switch',
])

WAFFLE_NAMES_ARG_POS = {
    'flag_is_active': 1,
    'switch_is_active': 0,
    'sample_is_active': 0,
    'waffle_flag': 0,
    'waffle_switch': 0,
}

class Router(ToggleArea):
    def __init__(self, file, start, end, an_ast):
        self._cleanupregex = None

        # Strip django templates features not related to Routers, but to other components.
        # This is necessary to increase the chances to find common Routers.
        if ToggleArea.isasttype(an_ast, 'django-template'):
            temp_ast = copy.copy(an_ast)
            temp_ast.child_nodelists = ()
            temp_ast.nodelist_false = []
            temp_ast.nodelist_true = []
            an_ast = temp_ast

        super(Router, self).__init__(file, start, end, an_ast)

    def __copy__(self):
        newone = type(self)(self.file, self.start, self.end, self.ast)
        newone.__dict__.update(self.__dict__)
        delattr(newone, '_cleanupregex')
        return newone

    def id(self):
        hash = hashlib.sha256()
        hash.update(bytes(self.file, ENCODING))
        hash.update(bytes(Router.ast_as_string(self.ast), ENCODING))
        return '{name}-{hash}'.format(name=self.__get_name(), hash=hash.hexdigest())

    def common_id(self):
        hash = hashlib.sha256()
        hash.update(bytes(Router.ast_as_string(self.ast, with_positions=False), ENCODING))
        return '{name}-{hash}'.format(name=self.__get_name(), hash=hash.hexdigest())

    def _cleanup_name(self, name):
        if self._cleanupregex is None:
            self._cleanupregex = re.compile(r'[^a-z0-9-\._]', re.IGNORECASE)

        return re.sub(self._cleanupregex, '', name)

    def __get_name(self):
        if Router.isasttype(self.ast, 'py'):
            return self.__get_py_ast_name()
        elif Router.isasttype(self.ast, 'django-template'):
            return self.__get_django_template_ast_name()

    def __get_py_ast_name(self):
        method_name = self.ast.func.attr if hasattr(self.ast.func, 'attr') else self.ast.func.id
        arg = self.ast.args[WAFFLE_NAMES_ARG_POS[method_name]]
        name = None
        if isinstance(arg, ast.Str):
            name = arg.s
        elif isinstance(arg, ast.Name):
            name = arg.id
        else:
            name = astor.to_source(arg, indent_with='').strip()

        return self._cleanup_name(name)

    def __get_django_template_ast_name(self):
        return self._cleanup_name(self.ast.name)

    @staticmethod
    def is_router(node):
        if Router.isasttype(node, 'py'):
            return Router.is_router_py(node)
        elif Router.isasttype(node, 'django-template'):
            return Router.is_router_django_template(node)

    @staticmethod
    def is_router_py(node):
        return \
            isinstance(node, ast.Call) and \
            (
                (
                    hasattr(node.func, 'attr') and \
                    len(node.args) > 0 and \
                    node.func.attr in WAFFLE_ROUTERS
                ) or \
                (
                    hasattr(node.func, 'id') and \
                    len(node.args) > 0 and \
                    node.func.id in WAFFLE_ROUTERS
                )
            )

    @staticmethod
    def is_router_django_template(node):
        return isinstance(node, WaffleNode)

class Point(ToggleArea):
    def __init__(self, file, start, end, an_ast):
        super(Point, self).__init__(file, start, end, an_ast)

    def id(self):
        hash = hashlib.sha256()
        hash.update(bytes(self.file, ENCODING))
        hash.update(bytes(Point.ast_as_string(self.ast), ENCODING))
        return hash.hexdigest()

    def common_id(self):
        if Point.isasttype(self.ast, 'py'):
            return self.__common_id_py()
        elif Point.isasttype(self.ast, 'django-template'):
            return self.__common_id_django_template()

    def __get_decorator(self):
        waffle_decorator = None
        for decorator in self.ast.decorator_list:
            if Router.is_router(decorator):
                waffle_decorator = decorator
                break

        if waffle_decorator is None:
            raise LookupError('No waffle decorator was found')

        return waffle_decorator

    def __common_id_py(self):
        noBodyAst = None
        if isinstance(self.ast, ast.If):
            ifStmt = copy.copy(self.ast)
            # Can't delete these attributes, so lets reset
            ifStmt.body = []
            ifStmt.orelse = []
            noBodyAst = ifStmt
        elif isinstance(self.ast, ast.FunctionDef):
            noBodyAst = self.__get_decorator()
        else:
            raise TypeError('Node type {0} not supported'.format(self.ast.__class__.__name__))

        hash = hashlib.sha256()
        hash.update(bytes(Point.ast_as_string(noBodyAst, with_positions=False), ENCODING))
        return hash.hexdigest()

    def __common_id_django_template(self):
        temp_ast = copy.copy(self.ast)
        # Can't delete these attributes, so lets reset
        temp_ast.nodelist_false = []
        temp_ast.nodelist_true = []

        hash = hashlib.sha256()
        hash.update(bytes(Point.ast_as_string(temp_ast, with_positions=False), ENCODING))
        return hash.hexdigest()

    @staticmethod
    def is_point(node):
        if isinstance(node, ast.If):
            return Point.check_ifstmt_has_router(node.test)
        elif isinstance(node, ast.FunctionDef):
            return Point.check_fndef_has_router(node)
        elif Point.isasttype(node, 'django-template'):
            return True
        else:
            return False

    @staticmethod
    def check_ifstmt_has_router(node):
        if isinstance(node, ast.BoolOp):
            return (
                Point.check_ifstmt_has_router(node.values[0]) or
                Point.check_ifstmt_has_router(node.values[1])
            )
        elif isinstance(node, ast.UnaryOp):
            return Point.check_ifstmt_has_router(node.operand)
        else:
            return Router.is_router(node)

    @staticmethod
    def check_fndef_has_router(node):
        if hasattr(node, 'decorator_list'):
            for decorator in node.decorator_list:
                if Router.is_router(decorator):
                    return True

        return False

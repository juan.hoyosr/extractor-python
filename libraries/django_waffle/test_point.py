import unittest
import re
import ast
from .loader import load_django_template
from .components import Point

class TestPoint(unittest.TestCase):

    def get_expression(self):
        return """
if foo and obj.flag_is_active(router, 'toggle-name') or bar:
    pass
"""

    def get_ast(self, code, position=0):
        return ast.parse(code).body[position]

    def get_template_ast(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% flag "a_toggle" %}',
            '    flag_name is active!',
            '{% endflag %}'
        ])
        return load_django_template(template).nodelist[2]

    def assert_is_point(self, an_ast):
        self.assertTrue(Point.is_point(an_ast))

    def test_id(self):
        expression = self.get_expression()
        point = Point('foo.py', None, None, self.get_ast(expression))
        self.assertTrue(re.search(r'^[a-z0-9]{64}$', point.id), msg='{0} does not have the correct format id'.format(point.id))
    
    def test_waffle_tag_id(self):
        point = Point('foo.py', None, None, self.get_template_ast())
        self.assertTrue(re.search(r'^[a-z0-9]{64}$', point.id), msg='{0} does not have the correct format id'.format(point.id))

    def test_same_common_id_when_different_files(self):
        point_one = Point('foo.py', None, None, self.get_ast(self.get_expression()))
        point_two = Point('bar.py', None, None, self.get_ast(self.get_expression()))
        self.assertEqual(point_one.common_id, point_two.common_id)

    def test_same_common_id_when_same_file(self):
        point_one = Point('foo.py', None, None, self.get_ast(self.get_expression()))
        point_two = Point('foo.py', None, None, self.get_ast(self.get_expression()))
        self.assertEqual(point_one.common_id, point_two.common_id)

    def test_same_common_id_of_decorator_when_same_file(self):
        code = '\n'.join([
            '@waffle_flag(\'toggle-A\')',
            'def a_view(request):',
            '    pass',
            '@waffle_flag(\'toggle-A\')',
            'def a_view(request):',
            '    pass'
        ])

        point_one = Point('foo.py', None, None, self.get_ast(code, 0))
        point_two = Point('foo.py', None, None, self.get_ast(code, 1))
        self.assertEqual(point_one.common_id, point_two.common_id)

    def test_same_common_id_of_flag_tags_when_same_file(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% flag "toggle_a" %}',
            '    do something here',
            '{% endflag %}'
            '{% flag "toggle_a" %}',
            '    and something different here',
            '{% endflag %}'
        ])
        nodelist = load_django_template(template).nodelist
        point_one = Point('foo.py', None, None, nodelist[2])
        point_two = Point('foo.py', None, None, nodelist[3])
        self.assertEqual(point_one.common_id, point_two.common_id)

    def test_is_point_when_lonely_test(self):
        expression = """
if obj.flag_is_active(router, 'toggle-name'):
    pass
"""
        self.assert_is_point(self.get_ast(expression))

    def test_is_point_when_logical_operation(self):
        expression = self.get_expression()
        self.assert_is_point(self.get_ast(expression))

    def test_is_point_when_unary_operation(self):
        expression = """
if not obj.flag_is_active(router, 'toggle-name'):
    pass
"""
        self.assert_is_point(self.get_ast(expression))

    def test_is_point_when_a_decorated_method(self):
        code = """
@waffle_flag('toggle-B')
def a_view(request):
    pass
"""
        self.assert_is_point(self.get_ast(code))

    def test_is_point_when_a_waffle_tag(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% flag "a_toggle" %}',
            '    flag_name is active!',
            '{% endflag %}'
        ])
        self.assert_is_point(self.get_template_ast())

if __name__ == '__main__':
    unittest.main()
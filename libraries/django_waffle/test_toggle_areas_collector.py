import unittest
import ast
from .collectors import PyAstCollector, DjangoTemplateCollector
from .components import Point, Router
from .loader import load_django_template

class TestToggleCollector(unittest.TestCase):

    def assertToggleAttrs(self, toggle, attrs):
        toggle_attrs = { key: getattr(toggle, key) for key, value in attrs.items() if hasattr(toggle, key) }
        self.assertDictEqual(toggle_attrs, attrs)

    def test_collects_toggle_areas(self):
        expression = """
a = 3
if obj.flag_is_active(router, 'toggle-A'):
    pass
"""
        node = ast.parse(expression)
        collector = PyAstCollector('foo.py', node)
        toggles = collector.get_toggles()
        self.assertEqual(len(toggles), 2)
        self.assertTrue(isinstance(toggles[0], Point))
        self.assertToggleAttrs(toggles[0], {
            'file': 'foo.py',
            'start': {
                'line': 3,
                'column': 0,
            },
            'end': {
                'line': 4,
                'column': 4,
            },
        })

        self.assertTrue(isinstance(toggles[1], Router))
        self.assertToggleAttrs(toggles[1], {
            'file': 'foo.py',
            'start': {
                'line': 3,
                'column': 3,
            },
            'end': {
                'line': 3,
                # TODO: should be 41
                'column': 3,
            },
        })

    def test_collects_toggle_areas_from_decorators(self):
        expression = """
b = 4
@waffle_flag('toggle-B')
def a_view(request):
    pass
"""
        node = ast.parse(expression)
        collector = PyAstCollector('foo.py', node)
        toggles = collector.get_toggles()
        self.assertEqual(len(toggles), 2)
        self.assertTrue(isinstance(toggles[0], Point))
        self.assertToggleAttrs(toggles[0], {
            'file': 'foo.py',
            'start': {
                'line': 3,
                'column': 0
            },
            'end': {
                'line': 5,
                'column': 4
            },
        })

        self.assertTrue(isinstance(toggles[1], Router))
        self.assertToggleAttrs(toggles[1], {
            'file': 'foo.py',
            'start': {
                'line': 3,
                'column': 1,
            },
            'end': {
                'line': 3,
                # TODO: should be 24
                'column': 1,
            },
        })

    def test_collects_toggle_areas_from_templates(self):
        source = """
{% load waffle_tags %}
{% flag "a_toggle" %}
    flag_name is active!
{% endflag %}
"""
        template = load_django_template(source)
        collector = DjangoTemplateCollector('foo.html', template)
        toggles = collector.get_toggles()
        self.assertEqual(len(toggles), 2)
        self.assertTrue(isinstance(toggles[0], Point))
        self.assertToggleAttrs(toggles[0], {
            'file': 'foo.html',
            'start': {
                'line': 3,
            },
            'end': {
                'line': 5,
            },
        })

        self.assertTrue(isinstance(toggles[1], Router))
        self.assertToggleAttrs(toggles[1], {
            'file': 'foo.html',
            'start': {
                'line': 3,
            },
            'end': {
                'line': 3,
            },
        })

    def test_collects_toggle_areas_from_templates_with_inner_unknown_tags(self):
        source = """
{% load waffle_tags %}
{% flag "toggle_A" %}
    flag_name is active!
    {% unknown_tag_A %}{% unknown_tag_B %} l;k lkm;lk
{% endflag %}
"""
        template = load_django_template(source)
        collector = DjangoTemplateCollector('foo.html', template)
        toggles = collector.get_toggles()
        self.assertEqual(len(toggles), 2)
        self.assertTrue(isinstance(toggles[0], Point))
        self.assertToggleAttrs(toggles[0], {
            'file': 'foo.html',
            'start': {
                'line': 3,
            },
            'end': {
                'line': 6,
            },
        })

        self.assertTrue(isinstance(toggles[1], Router))
        self.assertToggleAttrs(toggles[1], {
            'file': 'foo.html',
            'start': {
                'line': 3,
            },
            'end': {
                'line': 3,
            },
        })

    def test_collects_toggle_areas_from_complex_templates(self):
        source = """
{% load waffle_tags %}
{% if True %}
    {% flag "toggle_A" %}
        flag_name is active!
        <link href="{% if "foo" %}foo.html{% else %}bar.html{% endif %}" />
        {% if "baz" %}
        <h2>Hi there!</h2>
        <p>I'm baz!</p>
        {% endif %}
    {% endflag %}
{% else %}
    {% switch "!toggle_B" %}
        switch active
    {% else %}
        switch not active
    {% endswitch %}
{% endif %}
"""
        template = load_django_template(source)
        collector = DjangoTemplateCollector('foo.html', template)
        toggles = collector.get_toggles()
        self.assertEqual(len(toggles), 4)
        self.assertTrue(isinstance(toggles[0], Point))
        self.assertToggleAttrs(toggles[0], {
            'file': 'foo.html',
            'start': {
                'line': 4,
            },
            'end': {
                'line': 11,
            },
        })

        self.assertTrue(isinstance(toggles[1], Router))
        self.assertToggleAttrs(toggles[1], {
            'file': 'foo.html',
            'start': {
                'line': 4,
            },
            'end': {
                'line': 4,
            },
        })

        self.assertTrue(isinstance(toggles[2], Point))
        self.assertToggleAttrs(toggles[2], {
            'file': 'foo.html',
            'start': {
                'line': 13,
            },
            'end': {
                'line': 17,
            },
        })

        self.assertTrue(isinstance(toggles[3], Router))
        self.assertToggleAttrs(toggles[3], {
            'file': 'foo.html',
            'start': {
                'line': 13,
            },
            'end': {
                'line': 13,
            },
        })


if __name__ == '__main__':
    unittest.main()
import unittest
import ast
import re
from .components import ToggleArea

class TestToggleArea(unittest.TestCase):

    def get_hash(self, source):
        node = ast.parse(source).body[0]
        ast.fix_missing_locations(node)
        return ToggleArea('foo.py', None, None, node).hash

    def test_computes_a_hash(self):
        hash = self.get_hash("obj.feature_is_active(router, 'toggle-name')")
        self.assertTrue(re.search(r'^[a-z0-9]{64}$', hash), msg='{0} does not have the correct hash format'.format(hash))

if __name__ == '__main__':
    unittest.main()
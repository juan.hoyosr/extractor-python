import unittest
import re
import ast
from .loader import load_django_template
from .components import Router

class TestRouter(unittest.TestCase):
    def get_ast(self, code, a_type='expression', bodypart=0, listpart=0, nodelistpart=2):
        if a_type == 'template':
            an_ast = load_django_template(code).nodelist[nodelistpart]
        else:
            an_ast = ast.parse(code).body[bodypart]
            if a_type == 'expression':
                an_ast = an_ast.value
            elif a_type == 'function_def':
                an_ast = an_ast.decorator_list[listpart]
        return an_ast

    def assert_id(self, an_ast, toggle_name=r'[a-z0-9\-_]+'):
        router = Router('foo.py', None, None, an_ast)
        # Escape and remove statement split characters (\) and whitespaces
        escaped_toggle_name = re.escape(re.sub(r'\\|\s', '', toggle_name))
        self.assertTrue(re.search(r'^' + escaped_toggle_name + r'-[a-z0-9]{64}$', router.id), msg='{0} does not have the correct format id'.format(router.id))

    def is_router(self, method, a_type='expression'):
        return Router.is_router(self.get_ast(method, a_type))

    def test_id_flag_is_active(self):
        toggle_name = 'toggle_as_variable'
        self.assert_id(
            self.get_ast("obj.flag_is_active(request, '{0}')".format(toggle_name)),
            toggle_name
        )

    def test_id_switch_is_active(self):
        toggle_name = 'toggle_as.ATTRIBUTE'
        self.assert_id(
            self.get_ast("obj.switch_is_active({0})".format(toggle_name)),
            toggle_name
        )

    def test_id_switch_is_active_str_with_dashes(self):
        toggle_name = '"a-string-with-dashes"'
        self.assert_id(
            self.get_ast("obj.switch_is_active({0})".format(toggle_name)),
            'a-string-with-dashes'
        )

    def test_id_switch_is_active_concat(self):
        toggle_name = 'prefix + suffix'
        self.assert_id(
            self.get_ast("obj.switch_is_active({0})".format(toggle_name)),
            'prefixsuffix'
        )

    def test_id_switch_is_active_sequence_part(self):
        toggle_name = 'variable[1:]'
        self.assert_id(
            self.get_ast("obj.switch_is_active({0})".format(toggle_name)),
            'variable1'
        )

    def test_id_sample_is_active(self):
        toggle_name = '\n'.join([
            'toggle_as.method(',
            '    foo',
            ')'
        ])
        self.assert_id(
            self.get_ast("obj.sample_is_active({0})".format(toggle_name)),
            'toggle_as.methodfoo'
        )

    def test_id_flag_is_active_decorator(self):
        toggle_name = 'toggle_as_variable'
        code = '\n'.join([
            "@waffle_flag('{0}', 'url_redirect_to')",
            "def a_view(request):",
            "    pass"
        ]).format(toggle_name)
        self.assert_id(self.get_ast(code, 'function_def'), toggle_name)

    def test_id_switch_is_active_decorator(self):
        toggle_name = 'toggle_as_variable'
        code = '\n'.join([
            "@waffle_switch('{0}')",
            "def a_view(request):",
            "    pass"
        ]).format(toggle_name)
        self.assert_id(self.get_ast(code, 'function_def'), toggle_name)

    def test_id_inverted_decorator(self):
        toggle_name = 'toggle_as_variable'
        code = '\n'.join([
            "@waffle_switch('!{0}')",
            "def a_view(request):",
            "    pass"
        ]).format(toggle_name)
        self.assert_id(self.get_ast(code, 'function_def'), toggle_name)

    def test_id_flag_tag(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% flag "a_toggle" %}',
            '    flag_name is active!',
            '{% endflag %}'
        ])
        self.assert_id(self.get_ast(template, 'template'), 'a_toggle')

    def test_id_switch_tag(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% switch "a_toggle" %}',
            '    switch_name is active!',
            '{% endswitch %}'
        ])
        self.assert_id(self.get_ast(template, 'template'), 'a_toggle')

    def test_id_sample_tag(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% sample "a_toggle" %}',
            '    sample_name is active!',
            '{% endsample %}'
        ])
        self.assert_id(self.get_ast(template, 'template'), 'a_toggle')

    def test_id_sample_tag_with_single_quotes(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            "{% sample 'a_toggle' %}",
            '    sample_name is active!',
            '{% endsample %}'
        ])
        self.assert_id(self.get_ast(template, 'template'), 'a_toggle')

    def test_different_flag_is_active_ids(self):
        code = '\n'.join([
            "obj.flag_is_active(request, 'toggle_name')",
            "a = 1",
            "obj.flag_is_active(request, 'toggle_name')"
        ])
        router_one = Router('foo.py', None, None, self.get_ast(code, bodypart=0))
        router_two = Router('foo.py', None, None, self.get_ast(code, bodypart=2))
        self.assertNotEqual(router_one.id, router_two.id)

    def test_different_decorator_ids(self):
        code = '\n'.join([
            "@waffle_switch('toggle_name')",
            "def a_view(request):",
            "    pass",
            "@waffle_switch('toggle_name')",
            "def a_view(request):",
            "    pass",
        ])
        router_one = Router('foo.py', None, None, self.get_ast(code, 'function_def', bodypart=0))
        router_two = Router('foo.py', None, None, self.get_ast(code, 'function_def', bodypart=1))
        self.assertNotEqual(router_one.id, router_two.id)

    def test_different_flag_tag_ids(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% flag "toggle_a" %}',
            '    do something here',
            '{% endflag %}',
            '<!-- Both routers do the same, but in different locations -->'
            '{% flag "toggle_a" %}',
            '    do something here',
            '{% endflag %}'
        ])
        router_one = Router('foo.py', None, None, self.get_ast(template, a_type='template', nodelistpart=2))
        router_two = Router('foo.py', None, None, self.get_ast(template, a_type='template', nodelistpart=4))
        self.assertNotEqual(router_one.id, router_two.id)

    def get_expression(self, method='flag_is_active', toggle_name="'toggle-name'"):
        if method == 'flag_is_active':
            return "obj.{0}(request, {1})".format(method, toggle_name)
        else:
            return "obj.{0}({1})".format(method, toggle_name)

    def test_same_common_id_when_different_files(self):
        router_one = Router('foo.py', None, None, self.get_ast(self.get_expression()))
        router_two = Router('bar.py', None, None, self.get_ast(self.get_expression()))
        self.assertEqual(router_one.common_id, router_two.common_id)

    def test_same_common_id_when_same_files(self):
        router_one = Router('foo.py', None, None, self.get_ast(self.get_expression()))
        router_two = Router('foo.py', None, None, self.get_ast(self.get_expression()))
        self.assertEqual(router_one.common_id, router_two.common_id)

    def test_same_common_id_when_different_lines(self):
        router_one = Router('foo.py', None, None, self.get_ast("obj.flag_is_active(request, 'bar')"))
        router_two = Router('foo.py', None, None, self.get_ast('\n'.join([
            "baz=1",
            "obj.flag_is_active(request, 'bar')"
        ]), bodypart=1))
        self.assertEqual(router_one.common_id, router_two.common_id)

    def test_same_common_id_in_flag_tag_when_different_lines(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% flag "toggle_a" %}',
            '    do something here',
            '{% endflag %}'
            '{% flag "toggle_a" %}',
            '    and something different here',
            '{% endflag %}'
        ])
        router_one = Router('foo.py', None, None, self.get_ast(template, a_type='template', nodelistpart=2))
        router_two = Router('foo.py', None, None, self.get_ast(template, a_type='template', nodelistpart=3))
        self.assertEqual(router_one.common_id, router_two.common_id)

    def test_is_router_flag_is_active(self):
        self.assertTrue(self.is_router("obj.flag_is_active(request, 'toggle-name')"))

    def test_is_router_switch_is_active(self):
        self.assertTrue(self.is_router("obj.switch_is_active('toggle-name')"))

    def test_is_router_sample_is_active(self):
        self.assertTrue(self.is_router("obj.sample_is_active('toggle-name')"))

    def test_is_router_flag_is_active_no_instance(self):
        self.assertTrue(self.is_router("flag_is_active(request, 'toggle-name')"))

    def test_is_router_switch_is_active_no_instance(self):
        self.assertTrue(self.is_router("switch_is_active('toggle-name')"))

    def test_is_router_sample_is_active_no_instance(self):
        self.assertTrue(self.is_router("sample_is_active('toggle-name')"))

    def test_is_router_waffle_flag_decorator(self):
        code = '\n'.join([
            "@waffle_flag('toggle-name')",
            "def a_view(request):",
            "    pass"
        ])
        self.assertTrue(self.is_router(code, 'function_def'))

    def test_is_router_waffle_switch_decorator(self):
        code = '\n'.join([
            "@waffle_switch('toggle-name', 'url_redirect_to')",
            "def a_view(request):",
            "    pass"
        ])
        self.assertTrue(self.is_router(code, 'function_def'))

    def test_is_router_flag_tag(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% flag "a_toggle" %}',
            '    flag_name is active!',
            '{% endflag %}'
        ])
        self.assertTrue(self.is_router(template, 'template'))

    def test_is_router_switch_tag(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% switch "a_toggle" %}',
            '    switch_name is active!',
            '{% endswitch %}'
        ])
        self.assertTrue(self.is_router(template, 'template'))

    def test_is_router_sample_tag(self):
        template = '\n'.join([
            '{% load waffle_tags %}',
            '{% sample "a_toggle" %}',
            '    sample_name is active!',
            '{% endsample %}'
        ])
        self.assertTrue(self.is_router(template, 'template'))

    def test_is_not_router(self):
        # Wrapped routers not supported yet.
        # Example: https://github.com/edx/completion/blob/c64d73b00b8fef836c0950cba49413409b70419f/completion/waffle.py#L87
        self.assertFalse(self.is_router("waffle_flag()"))

    def test_is_not_router_as_obj_attribute(self):
        self.assertFalse(self.is_router("obj.waffle_flag()"))

if __name__ == '__main__':
    unittest.main()
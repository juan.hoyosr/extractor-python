import re
import py_parser
from .loader import load_django_template
from .collectors import PyAstCollector, DjangoTemplateCollector

class UnknownFileParser(Exception):
    """Exception raised when no parser is found for a file.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

PYTHON_FILE_EXTENSIONS = ['py']
TEMPLATE_FILE_EXTENSIONS = ['html', 'dj', 'djt']

def hasextensions(filename, extensions):
    regex = r'({0})$'.format('|'.join(extensions))
    return re.search(regex, filename, flags=re.IGNORECASE) is not None

def ispython(filename):
    return hasextensions(filename, PYTHON_FILE_EXTENSIONS)

def istemplate(filename):
    return hasextensions(filename, TEMPLATE_FILE_EXTENSIONS)

def parser(file, filename):
    # Keep using the filename relative to the project.
    # This will create shorter file references and will
    # make callers like `extractor` happy.
    file.relativename = filename
    if ispython(filename):
        toggles = parse_python(file)
    elif istemplate(filename):
        toggles = parse_template(file)
    else:
        raise UnknownFileParser('Cannot find a parser for {0}'.format(filename))

    return toggles

def cleanup(source):
    # Avoid null bytes and make the parsers happy
    return source.strip('\x00')

def parse_python(file):
    # Remove null bytes so ast.parse does not complain
    ast = py_parser.parse(cleanup(file.read()), file.relativename)
    collector = PyAstCollector(file.relativename, ast)
    return collector.get_toggles()

def parse_template(file):
    template = load_django_template(file.read())
    collector = DjangoTemplateCollector(file.relativename, template)
    return collector.get_toggles()
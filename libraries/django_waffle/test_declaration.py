import unittest
import re
from .components import Declaration

class TestRouter(unittest.TestCase):

    def test_id_flag(self):
        declaration = Declaration(name='a_feature', environment='all', waffle_type='flag')
        self.assertEqual('a_feature-all-flag', declaration.id)

if __name__ == '__main__':
    unittest.main()
from .django_waffle.parser import parser

parsers = {
    'django_waffle': parser,
}

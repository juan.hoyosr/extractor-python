import unittest
from unittest.mock import patch, mock_open
import file_parser

class TestFileParser(unittest.TestCase):

    def test_parse_an_expression(self):
        with patch('file_parser.open', mock_open(read_data='a = 0')) as mock:
            self.assertIsNotNone(file_parser.parse('foo.py', '~/here', 'django_waffle'))

    def test_parse_strips_null_bytes(self):
        with patch('file_parser.open', mock_open(read_data='a = 0\x00')) as mock:
            self.assertIsNotNone(file_parser.parse('foo.py', '~/here', 'django_waffle'))

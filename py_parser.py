import sys
import ast
from lib2to3 import refactor
from lib2to3 import fixes
from lib2to3.pgen2.parse import ParseError

def parse(source, filename):
    try:
        return ast.parse(source)
    except SyntaxError:
        # Maybe a Python2 syntax?
        py3_source = parse_from_py2(source, filename)
        return ast.parse(py3_source, filename)

avail_fixes = set(refactor.get_fixers_from_package('lib2to3.fixes'))

def parse_from_py2(source, filename):
    # TODO: subclass RefactoringTool
    refactor_tool = refactor.RefactoringTool(avail_fixes)
    input = source + "\n" # https://github.com/python/cpython/blob/5fd33b5926eb8c9352bf5718369b4a8d72c4bb44/Lib/lib2to3/refactor.py#L342
    concrete_st = refactor_tool.refactor_string(input, filename)
    # https://github.com/python/cpython/blob/e42b705188271da108de42b55d9344642170aa2b/Lib/lib2to3/refactor.py#L337
    return str(concrete_st)[:-1]

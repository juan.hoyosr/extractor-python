import sys
import json
from file_parser import parse

def ipc_parse():
    while True:
        line = sys.stdin.readline()
        message = json.loads(line)
        if message['action'] == 'parse':
            payload = message['payload']
            library = payload['library']
            filepath = payload['filepath']
            directory = payload['directory']
            print(parse(filepath, directory, library))
        elif message['action'] == 'end':
            break
        else:
            break
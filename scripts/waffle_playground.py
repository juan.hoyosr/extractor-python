"""
Waffle template tags playground

Setup:
$ pip install Django django-waffle
"""
import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '..'))
from libraries.django_waffle.loader import load_django_template

s = """
{% load waffle_tags %}
{% flag "flag_name" %}
    flag_name is active!
{% else %}
    flag_name is inactive
{% endflag %}
"""

ast = load_django_template(s)
"""
waffle_tag = ast.nodelist[3].conditions_nodelists[0][1][1]
module = waffle_tag.__module__
"""
print(ast)

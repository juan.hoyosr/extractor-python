"""
jinja2 template tags playground

Setup:
$ pip install Django django-waffle django-jinja
"""

import django
from django import template
from django.conf import settings
from django.template import engines

s = """
{% if something_special %}
    {% if waffle.flag('flag_name') %}
        flag_name is active!
    {% endif %}
{% else %}
    Nothing special here
{% endif %}
"""
django_config = {
    'INSTALLED_APPS': [
        # default django apps
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',

        'waffle',
        'django_jinja',
    ],
    'TEMPLATES': [
        {
            'BACKEND': 'django_jinja.backend.Jinja2',
            'APP_DIRS': True,
            'OPTIONS': {
                'extensions': [
                    'waffle.jinja.WaffleExtension',

                    # django-jinja "complete example" extensions
                    "jinja2.ext.do",
                    "jinja2.ext.loopcontrols",
                    "jinja2.ext.with_",
                    "jinja2.ext.i18n",
                    "jinja2.ext.autoescape",
                    "django_jinja.builtins.extensions.CsrfExtension",
                    "django_jinja.builtins.extensions.CacheExtension",
                    "django_jinja.builtins.extensions.TimezoneExtension",
                    "django_jinja.builtins.extensions.UrlsExtension",
                    "django_jinja.builtins.extensions.StaticFilesExtension",
                    "django_jinja.builtins.extensions.DjangoFiltersExtension",
                ],
            }
        }
    ],
    'USE_I18N': True
}
settings.configure(**django_config)
django.setup()
env = engines['backend'].env
# Compile to string
code = env.compile(s, name=None, filename=None)
string = env.template_class.from_code(env, code, env.make_globals({}), lambda: True)
print(string)

# jinja will build an ast to generate code that will output a string
# template -> ast -> code -> string
ast = env.parse(s)
"""
if_stmt = ast.body[1].body[1]
call = if_stmt.test
"""
generator = env.code_generator_class(env, name=None, filename=None, stream=None, defer_init=False)
generator.visit(ast)
code = generator.stream.getvalue()
print(code)
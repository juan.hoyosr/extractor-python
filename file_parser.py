import json
import os
import sys
import traceback
from libraries import parsers
from json import JSONEncoder
from copy import copy

def parse(filename, cwd, library):
    try:
        parser = parsers[library.replace('-', '_')]
    except KeyError as error:
        return as_error('ERROR (KeyError): {0}. No parser found for the given library'.format(error), filename)

    with open(os.path.join(cwd, filename), 'r') as file:
        try:
            toggles = parser(file, filename)
        except UnicodeDecodeError as error:
            # Files not encoded in utf-8 (Python 3's default)
            return as_error('WARN (UnicodeDecodeError): {0}'.format(error), filename)
        except SyntaxError as error:
            return as_error('WARN (SyntaxError): {0}'.format(error.msg), filename)
        except TabError as error:
            return as_error('WARN (TabError): {0}'.format(error.msg), filename)
        except:
            exec_info = sys.exc_info()
            etype = exec_info[0]
            value = exec_info[1]
            tb = exec_info[2]
            trace = ''.join(traceback.format_exception(etype, value, tb))
            return as_error('WARN (UndefinedError): {0}'.format(trace), filename)

        return as_toggles(toggles)

def as_error(msg, filename):
    return json.dumps([
        {
            '__error__': {
                'msg': msg,
                'filepath': filename,
            }
        }
    ])


class Encoder(JSONEncoder):
    def default(self, object):
        o = copy(object)
        if not hasattr(o, '__dict__'):
            return str(o)

        if 'ast' in o.__dict__:
            del o.__dict__['ast']

        return o.__dict__

def as_toggles(toggles):
    return json.dumps(toggles, cls=Encoder)